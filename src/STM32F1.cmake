################################################################################
### STM32F1 series definitions and functions.
#
# When included:
#	Set chip series and chip family, search for STM32F10x-StdPeriph-Lib package, include library's
#	header files, search for linker script and gdb init script.
#
# Need following variables:
#	STM32_CODE						: Code mark of the destination device chip.
#	STM32_STDPERIPH_VERSION			: Optional: Version of STM Standard Peripheral Driver library to link.
#	STM32_LINKER_SCRIPT				: Optional: Full path to the linker script.
#	STM32_GDB_SCRIPT				: Optional: Full path to the GDB load script.
#	STM32_REMOTE_SERVER				: Optional: Remote gdb server to connect (like OpenOCD).
#	STM32_DEBUG_INITIAL_STATE		: Optional: Debugging initial state (one of RUN/HALT/MAIN).
#
# Defines following variables:
#	STM32_SERIES					: STM32 series (ex. F0, F1, F2).
#	STM32_FAMILY					: STM32F10x family (ex. CL, LD, MD, HD).
#	STM32_LIBRARY					: STM32F10x library file (ex. libstm32f10x_hd.a).
#
# Defines following variables if not user defined:
#	STM32_LINKER_SCRIPT				: STM32F10x linker script to use.
#	STM32_GDB_SCRIPT				: GDB init script for debugging.
#
# Defines following public functions:
#	stm32_add_target				: Set all needed stuff to make a stm32 target
#
# Define following private functions:
#	stm32_configure_linker_script	: Configure linker script
#	stm32_configure_gdb_script		: Configure gdb script



################################################################################
### STM32F1.cmake

### Set series definition
set(STM32_SERIES "F1")

### Check if STM32_CODE is defined
if(NOT DEFINED STM32_CODE)
	message(FATAL_ERROR "STM32F1: No STM32_CODE defined.")
endif()
### Standardize chip's code
string(TOUPPER ${STM32_CODE} STM32_CODE)

### Obtain chip family
string(REGEX REPLACE "^STM32F(10[012357].[468BCDEFG]).+$" "\\1" __PART ${STM32_CODE})
if(__PART MATCHES "10[57]")
	set(STM32_FAMILY "CL")
elseif(__PART MATCHES "10[13].[CDE]")
	set(STM32_FAMILY "HD")
elseif(__PART MATCHES "100.[CDE]")
	set(STM32_FAMILY "HD_VL")
elseif(__PART MATCHES "10[123].[8B]")
	set(STM32_FAMILY "MD")
elseif(__PART MATCHES "100.[8B]")
	set(STM32_FAMILY "MD_VL")
elseif(__PART MATCHES "10[123].[46]")
	set(STM32_FAMILY "LD")
elseif(__PART MATCHES "100.[46]")
	set(STM32_FAMILY "LD_VL")
elseif(__PART MATCHES "10[13].[FG]")
	set(STM32_FAMILY "XL")
else()
	message(FATAL_ERROR "STM32F1: Unable to get STM32_FAMILY from '${STM32_CODE}'; check your STM32_CODE variable in your CMakeLists.txt.")
endif()
### Add selected series and family to definitions (needed by StdPeriph Lib)
add_definitions("-DSTM32F10X_${STM32_FAMILY}")

if(DEFINED STM32_STDPERIPH_VERSION)
	### Add StdPeriph definition usage (needed by StdPeriph Lib)
	add_definitions("-DUSE_STDPERIPH_DRIVER")

	### Find STM32F10x Standard Peripheral library
	find_package("STM32F10x-StdPeriph-Lib" ${STM32_STDPERIPH_VERSION} EXACT REQUIRED)
	message(STATUS "STM32F1: Using STM32F10x Standard Peripheral library v.${STM32F10x-StdPeriph-Lib_VERSION}.")
	
	### Include STM32 library headers
	include_directories(${STM32F10x-StdPeriph-Lib_INCLUDE_DIR})

	### Lowerize chip's family
	string(TOLOWER ${STM32_FAMILY} STMFAM)
	### Search library file for this chip type
	find_library(STM32_LIBRARY "libstm32f10x_${STMFAM}.a" PATHS ${STM32F10x-StdPeriph-Lib_LIB_DIR} NO_DEFAULT_PATH)
	if(NOT STM32_LIBRARY)
		message(FATAL_ERROR "STM32F1: Can't find STM32 Standard Peripheral Lib's file 'libstm32f10x_${STMFAM}.a'.")
	endif()
	message(STATUS "STM32F1: library file '${STM32_LIBRARY}'")

	### If user doesn't define a custom linker script, use the library one
	if(NOT DEFINED STM32_LINKER_SCRIPT)
		find_file(STM32_LINKER_SCRIPT "stm32f10x.ld.in" PATHS ${STM32F10x-StdPeriph-Lib_DATA_DIR} NO_DEFAULT_PATH)
		if(NOT STM32_LINKER_SCRIPT)
			message(FATAL_ERROR "STM32F1: Can't find STM32 Standard Peripheral Lib's linker script 'stm32f10x.ld.in'.")
		endif()
	endif()

	### If user doesn't define a custom gdb script, use the library one
	if(NOT DEFINED STM32_GDB_SCRIPT)
		find_file(STM32_GDB_SCRIPT "stm32f10x.gdb.in" PATHS ${STM32F10x-StdPeriph-Lib_DATA_DIR} NO_DEFAULT_PATH)
		if(NOT STM32_GDB_SCRIPT)
			message(FATAL_ERROR "STM32F1: Can't find STM32 Standard Peripheral Lib's GDB script 'stm32f10x.gdb.in'.")
		endif()
	endif()
endif()



################################################################################
### stm32_add_target
# Add optional standard peripheral library from ST's as dependency for the main target, add an optional
# linker script (if not supplied, use a default one from library).
# Configure a gdb script for connecting to remote debugger and load target executable.
#
# Args:
#	TARGET	: Name of the main project's target.
function(stm32_add_target TARGET)
	message(STATUS "STM32F1: Add target '${TARGET}'")
	
	### Check if user define a STM32F10x Standard Peripheral library version to use
	if(DEFINED STM32_STDPERIPH_VERSION)
		### Link library
		target_link_libraries(${TARGET} ${STM32_LIBRARY})
		message(STATUS "STM32F1: library linked to target '${TARGET}'")
		### Use library startup file
		set_target_properties(${TARGET} PROPERTIES LINK_FLAGS "-nostartfiles")
	endif()
	
	### If we have a linker script
	if(DEFINED STM32_LINKER_SCRIPT)
		message(STATUS "STM32F1: linker script '${STM32_LINKER_SCRIPT}'")
		### Configure it and add to target
		stm32_configure_linker_script(${STM32_LINKER_SCRIPT} "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.ld")
		set_target_properties(${TARGET} PROPERTIES LINK_FLAGS "-T${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.ld")
	endif()
	
	### If we have a gdb script
	if(DEFINED STM32_GDB_SCRIPT)
		message(STATUS "STM32F1: GDB script '${STM32_GDB_SCRIPT}'")
		### Configure it and copy to build directory
		stm32_configure_gdb_script(${STM32_GDB_SCRIPT} "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.gdb" ${TARGET})
	endif()
endfunction()



################################################################################
### stm32_get_linker_variables
# Get linker variables to configure linker file.
#
# Args:
#	FILE_IN		: Input linker script to configure
#	FILE_OUT	: Output file of linker script
function(stm32_configure_linker_script FILE_IN FILE_OUT)
	### Get relevant parts of chip's code
	string(REGEX REPLACE "^STM32F10[012357].([468BCDEFG]).+$" "\\1" SIZE_PART ${STM32_CODE})
	string(REGEX REPLACE "^STM32F(10[012357]).[468BCDEFG].+$" "\\1" TYPE_PART ${STM32_CODE})
	
	### Obtain flash size
	if(SIZE_PART STREQUAL "4")
		set(FLASH_SIZE "16K")
	elseif(SIZE_PART STREQUAL "6")
		set(FLASH_SIZE "32K")
	elseif(SIZE_PART STREQUAL "8")
		set(FLASH_SIZE "64K")
	elseif(SIZE_PART STREQUAL "B")
		set(FLASH_SIZE "128K")
	elseif(SIZE_PART STREQUAL "C")
		set(FLASH_SIZE "256K")
	elseif(SIZE_PART STREQUAL "D")
		set(FLASH_SIZE "384K")
	elseif(SIZE_PART STREQUAL "E")
		set(FLASH_SIZE "512K")
	elseif(SIZE_PART STREQUAL "F")
		set(FLASH_SIZE "768K")
	elseif(SIZE_PART STREQUAL "G")
		set(FLASH_SIZE "1024K")
	else()
		message(FATAL_ERROR "STM32F1: Unable to get FLASH_SIZE from '${STM32_CODE}'.")
	endif()

	### Obtain ram size
	if(${STM32_FAMILY} STREQUAL "XL")
		set(RAM_SIZE "96K")
	elseif(${STM32_FAMILY} STREQUAL "CL")
		set(RAM_SIZE "64K")
	elseif((${STM32_FAMILY} STREQUAL "LD") AND ((TYPE_PART STREQUAL "102") OR (TYPE_PART STREQUAL "101")))
		if(SIZE_PART STREQUAL "4")
			set(RAM_SIZE "4K")
		else()
			set(RAM_SIZE "6K")
		endif()
	elseif(${STM32_FAMILY} STREQUAL "LD")
		if(SIZE_PART STREQUAL "4")
			set(RAM_SIZE "6K")
		else()
			set(RAM_SIZE "10K")
		endif()
	elseif(${STM32_FAMILY} STREQUAL "LD_VL")
		set(RAM_SIZE "4K")
	elseif((${STM32_FAMILY} STREQUAL "MD") AND ((TYPE_PART STREQUAL "102") OR (TYPE_PART STREQUAL "101")))
		if(SIZE_PART STREQUAL "8")
			set(RAM_SIZE "10K")
		else()
			set(RAM_SIZE "16K")
		endif()
	elseif(${STM32_FAMILY} STREQUAL "MD")
		set(RAM_SIZE "20K")
	elseif(${STM32_FAMILY} STREQUAL "MD_VL")
		set(RAM_SIZE "8K")
	elseif((${STM32_FAMILY} STREQUAL "HD") AND (TYPE_PART STREQUAL "101"))
		if(SIZE_PART STREQUAL "C")
			set(RAM_SIZE "32K")
		else()
			set(RAM_SIZE "48K")
		endif()
	elseif(${STM32_FAMILY} STREQUAL "HD")
		if(SIZE_PART STREQUAL "C")
			set(RAM_SIZE "48K")
		else()
			set(RAM_SIZE "64K")
		endif()
	elseif(${STM32_FAMILY} STREQUAL "HD_VL")
		if(SIZE_PART STREQUAL "C")
			set(RAM_SIZE "24K")
		else()
			set(RAM_SIZE "32K")
		endif()
	else()
		message(FATAL_ERROR "STM32F1: Unable to get RAM_SIZE from '${STM32_CODE}'.")
	endif()

	### Obtain end stack address
	if(RAM_SIZE STREQUAL "4K")
		set(END_STACK "0x20001000")
	elseif(RAM_SIZE STREQUAL "6K")
		set(END_STACK "0x20001800")
	elseif(RAM_SIZE STREQUAL "8K")
		set(END_STACK "0x20002000")
	elseif(RAM_SIZE STREQUAL "10K")
		set(END_STACK "0x20002800")
	elseif(RAM_SIZE STREQUAL "16K")
		set(END_STACK "0x20004000")
	elseif(RAM_SIZE STREQUAL "20K")
		set(END_STACK "0x20005000")
	elseif(RAM_SIZE STREQUAL "24K")
		set(END_STACK "0x20006000")
	elseif(RAM_SIZE STREQUAL "32K")
		set(END_STACK "0x20008000")
	elseif(RAM_SIZE STREQUAL "48K")
		set(END_STACK "0x2000C000")
	elseif(RAM_SIZE STREQUAL "64K")
		set(END_STACK "0x20010000")
	elseif(RAM_SIZE STREQUAL "96K")
		set(END_STACK "0x20018000")
	endif()

	### Obtain minimum stack size
	if(RAM_SIZE STRLESS "40K")
		set(MIN_STACK "0x100")
	else()
		set(MIN_STACK "0x200")
	endif()

	### Configure linker script
	# Need this variables defined:
	#	FLASH_SIZE, RAM_SIZE, END_STACK, MIN_STACK
	message(STATUS "STM32F1: Chip '${STM32_CODE}' has ${FLASH_SIZE}iB of FLASH memory and ${RAM_SIZE}iB of RAM memory.")
	configure_file(${FILE_IN} ${FILE_OUT})
endfunction()



################################################################################
### stm32_configure_gdb_script
# Get gdb variables to configure gdb file.
#
# Args:
#	FILE_IN		: Input gdb script to configure
#	FILE_OUT	: Output file of gdb script
#	TARGETFILE	: Name of the target file
function(stm32_configure_gdb_script FILE_IN FILE_OUT TARGETFILE)
		### Name of generated ELF file
		set(DEBUG_FILE ${CMAKE_CURRENT_BINARY_DIR}/${TARGETFILE})
		message(STATUS "STM32F1: file to debug '${DEBUG_FILE}'")

		### Check if defined a remote debug server
		if(NOT DEFINED STM32_DEBUG_REMOTE_SERVER)
			set(DEBUG_SERVER "localhost:3333")
		else()
			set(DEBUG_SERVER ${STM32_DEBUG_REMOTE_SERVER})
		endif()
		message(STATUS "STM32F1: remote server '${DEBUG_SERVER}'")
		
		### Check if defined a debug initial state
		if(NOT DEFINED STM32_DEBUG_INITIAL_STATE)
			set(DEBUG_INITIAL_STATE "RUN")
		else()
			set(DEBUG_INITIAL_STATE ${STM32_DEBUG_INITIAL_STATE})
		endif()
		message(STATUS "STM32F1: initial state '${DEBUG_INITIAL_STATE}'")
		if(DEBUG_INITIAL_STATE STREQUAL "RUN")
			set(DEBUG_INITIAL_STATE "continue")
		elseif(DEBUG_INITIAL_STATE STREQUAL "HALT")
			set(DEBUG_INITIAL_STATE "")
		elseif(DEBUG_INITIAL_STATE STREQUAL "MAIN")
			set(DEBUG_INITIAL_STATE "thbreak main\ncontinue")
		else()
			set(DEBUG_INITIAL_STATE "continue")
		endif()
		
		### Configure gdb script
		configure_file(${FILE_IN} ${FILE_OUT})
endfunction()
