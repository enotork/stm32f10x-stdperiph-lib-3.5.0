#ifndef _STM32F10x_CONF_H
#define _STM32F10x_CONF_H

// Check if debug is disabled by NDEBUG
#if !defined  NDEBUG

#define USE_FULL_ASSERT

#endif

// Uncomment/Comment the line below to enable/disable peripheral header file inclusion

// #include <stm32f10x_adc.h>
// #include <stm32f10x_bkp.h>
// #include <stm32f10x_can.h>
// #include <stm32f10x_cec.h>
// #include <stm32f10x_crc.h>
// #include <stm32f10x_dac.h>
// #include <stm32f10x_dbgmcu.h>
// #include <stm32f10x_dma.h>
// #include <stm32f10x_exti.h>
// #include <stm32f10x_flash.h>
// #include <stm32f10x_fsmc.h>
// #include <stm32f10x_gpio.h>
// #include <stm32f10x_i2c.h>
// #include <stm32f10x_iwdg.h>
// #include <stm32f10x_pwr.h>
// #include <stm32f10x_rcc.h>
// #include <stm32f10x_rtc.h>
// #include <stm32f10x_sdio.h>
// #include <stm32f10x_spi.h>
// #include <stm32f10x_tim.h>
// #include <stm32f10x_usart.h>
// #include <stm32f10x_wwdg.h>
// #include <misc.h>

#ifdef USE_FULL_ASSERT

/** @def assert_param(expr)
 *  @brief  The assert_param macro is used for function's parameters check.
 *  @param  expr : If expr is false, it calls assert_failed function which reports
 *          the name of the source file and the source line number of the call
 *          that failed. If expr is true, it returns no value.
 */
#define assert_param(expr) ((expr) ? (void)0 : assert_failed( (uint8_t *)__FILE__, __LINE__ ))

/** @fn assert_failed( uint8_t* file, uint32_t line )
 *  @brief  Dummy function. Reports the name of the source file and the source line number
 *          where the assert_param error has occurred.
 *  @param  file : pointer to the source file name
 *  @param  line : assert_param error line source number
 */
void assert_failed( uint8_t* file, uint32_t line )__attribute__( ( weak ) );

#else

#define assert_param(expr) ((void)0)

#endif // USE_FULL_ASSERT

#endif // _STM32F10x_CONF_H
