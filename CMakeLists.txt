##### Readme ###################################################################
# You have to define at least this variables in your build system (ex. -D.......):
#	CMAKE_BUILD_TYPE			"Release"
#	CMAKE_INSTALL_PREFIX		"/usr/arm-none-eabi/usr"
#	CMAKE_TOOLCHAIN_FILE		"/usr/share/cmake/Modules/Toolchain/STM32F1.cmake"
cmake_minimum_required(VERSION 3.0)

##### Project ##################################################################
project("STM32F10x-StdPeriph-Lib" VERSION 3.5.0 LANGUAGES C ASM)
add_compile_options("-Wall")

##### Processing ###############################################################
add_subdirectory("src")
